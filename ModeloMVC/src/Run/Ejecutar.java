package Run;

import controller.Condicionales;
import controller.ControllerPersona;
import model.Persona;

/**
 * 
 * @author dareyes@externosdeloittemx.com
 *@version 1.0
 * @since 2020-03-11
 */
public class Ejecutar {

	/**
	 * Clase para mandar llamar los metodos y comprobar funcionalidad.
	 * @param args
	 */
	public static void main(String[] args) {
		
		Persona objeto1 = new Persona("Mariana", "Reyes", "Velazquez", "femenino", "REVD96", "Mecatronica",
				23, 1, 1, 3, 4, 5, 6, 8, 9, 14, 34, 45, 67, "String");
	
		
		String mayor_numero = Condicionales.numeroMayor(3,5);
		System.out.println(mayor_numero);
		
		String resta = Condicionales.Resta(3,5);
		System.out.println(resta);
		
		String atributos = ControllerPersona.agregarAtributo("Daniela", "Reyes", "Velazquez", "femenino", "REVD96", "Mecatronica",
				23, 1, 1, 3, 4, 5, 6, 8, 9, 14, 34, 45, 67, "String");
		System.out.println(atributos);
		
		String objetos = ControllerPersona.agregarObjeto(objeto1);
		System.out.println(objetos);

	}

}
