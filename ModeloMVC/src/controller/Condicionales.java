package controller;

/**
 * Esta clase contiene 5 metodos condiciones para numeros enteros.
 * 
 * @author dareyes@externosdeloittemx.com
 * @version 1.0
 * @since 2020-03-11
 */
public class Condicionales {

	// CREACION DE METODOS

	// CREACION DE METODOS DE TIPO TERNARIO

	/**
	 * Este metodo compara dos numeros e imprime el numero mayor.
	 * 
	 * @param a Debe ser de tipo int.
	 * @param b Debe ser de tipo int.
	 * @return Retorna una variable de tipo String.
	 */
	public static String numeroMayor(int a, int b) {
		String mensaje = (a > b) ? "El numero mayor es: " + a : "El numero mayor es: " + b;

		return mensaje;

	}

	/**
	 * Este metodo compara dos numeros, si el primero es mayor al segundo, realiza
	 * la resta. Si no lo es, manda mensaje de que el resultado es negativo y no
	 * hace operacion.
	 * 
	 * @param a Debe ser de tipo int.
	 * @param b Debe ser de tipo int.
	 * @return Retorna una variable de tipo String.
	 */
	public static String Resta(int a, int b) {
		String mensaje = (a >= b) ? "Total resta: " + (a - b)
				: "El resultado de la resta es negativo. Intentalo de nuevo,";

		return mensaje;
	}

	/**
	 * Este metodo compara dos numeros, si el primero es mayor o igual al segundo,
	 * realiza la division. Si no lo es, manda mensaje de que el resultado debe ser
	 * mayor a 1.
	 * 
	 * @param a Debe ser de tipo int.
	 * @param b Debe ser de tipo int.
	 * @return Retorna una variable de tipo String.
	 */
	public static String Division(int a, int b) {
		String mensaje = (a >= b) ? "Total division: " + (a / b) : "El resultado no debe ser menor a 1.";

		return mensaje;

	}

	// CREACION DE METODO DE TIPO IF

	/**
	 * Este metodo realiza una multiplicacion si los valores de los parametros son
	 * mayores a 0.
	 * 
	 * @param a Debe ser de tipo int.
	 * @param b Debe ser de tipo int.
	 * @return Retorna una variable de tipo String.
	 */
	public String Multiplicacion(int a, int b) {
		String mensaje = " ";
		if ((a > 0) && (b > 0)) {
			mensaje = "Total multiplicacion: " + (a * b);
		} else {
			mensaje = "Ingresa numeros mayores a 0.";
		}
		return mensaje;
	}

	// CREACION DE METODO DE TIPO IF ELSE

	/**
	 * Este metodo realiza una multiplicacion si los numeros son mayores a 0 o si
	 * ambos son menores a 0.
	 * 
	 * @param a Debe ser de tipo int.
	 * @param b Debe ser de tipo int.
	 * @return Retorna una variable de tipo String.
	 */
	public String Mult(int a, int b) {
		String mensaje = " ";
		if ((a > 0) && (b > 0)) {
			mensaje = "Total multiplicacion: " + (a * b);
		} else if ((a > 0) || (b > 0)) {
			mensaje = "No se puede realizar la operacion.";
		} else if ((a < 0) && (b < 0)) {
			mensaje = "Total multiplicacion: " + (a * b);
		}
		return mensaje;
	}
}
