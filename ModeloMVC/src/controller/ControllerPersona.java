package controller;

import model.Persona;

/**
 * En esta clase se crean los metodos para la clase Persona.
 *
 * @author dareyes@externosdeloittemx.com
 * @version 1.0
 * @since 2020-03-11
 */
public class ControllerPersona extends Persona {

	// CREACION DE METODOS PARA LA CLASE PERSONA.

	/**
	 * Metodo para imprimir todos los registros con los atributos de la clase
	 * Persona.
	 * 
	 * @return Retorna variable de tipo String.
	 */
	public String buscarTodos() {
		String registros = " ";
		listaPersonas.forEach(n -> {
			System.out.println("Nombre: " + this.getNombre() + "Apellido Paterno: " + this.getApellido1()
					+ "Apellido Materno: " + this.getApellido2() + "Genero: " + this.getGenero() + "RFC: "
					+ this.getRfc() + "Carrera: " + this.getCarrera() + "Edad: " + this.getEdad()
					+ "Cantidad de hermanos: " + this.getHermanos() + "Cantidad de hermanas: " + this.getHermanas()
					+ "Peso: " + this.getPeso() + "Estatura: " + this.getEstatura() + "Atributos extras: "
					+ this.getAtributo1() + ", " + this.getAtributo2() + ", " + this.getAtributo2() + ", "
					+ this.getAtributo3() + ", " + this.getAtributo4() + ", " + this.getAtributo5() + ", "
					+ this.getAtributo6() + ", " + this.getAtributo7() + ", " + this.getAtributo8() + ", "
					+ this.getAtributo9());
		});
		return registros;
	}

	/**
	 * Metodo para imprimir los datos de un solo atributo
	 * 
	 * @param edad Debe ser de tipo entero.
	 * @return Retorna variable de tipo String.
	 */
	public String buscarAtributo(int edad) {

		String atributo = " ";
		listaPersonas.forEach(n -> {
			System.out.println("Edades registradas: " + this.getEdad());

		});
		return atributo;
	}

	/**
	 * Metodo para crear agregar un objeto con registros nuevos
	 * 
	 * @param registroNuevo objeto nuevo
	 * @return Retorna un mensaje.
	 */
	public static String agregarObjeto(Persona registroNuevo) {
		String mensaje = " ";
		Persona.listaPersonas.add(registroNuevo);
		mensaje = "Se agreg� el objeto correctamente a la lista.";

		return mensaje;

	}

	/**
	 * Metodo para crear un nuevos atributos a la lista
	 * 
	 * @param nombre
	 * @return
	 */
	public static String agregarAtributo(String nombre, String apellido1, String apellido2, String genero, String rfc,
			String carrera, int edad, int hermanos, int hermanas, int atributo1, int atributo2, int atributo3,
			int atributo4, int atributo5, int atributo6, int atributo7, int atributo8, double peso, double estatura,
			String atributo9) {
		String mensaje = " ";
		Persona objeto = new Persona(nombre, apellido1, apellido2, genero, rfc, carrera, edad, hermanos, hermanas,
				atributo1, atributo2, atributo3, atributo4, atributo5, atributo6, atributo7, atributo8, estatura, peso,
				atributo9);
		Persona.listaPersonas.add(objeto);
		mensaje = "Se agregaron los atributos correctamente.";

		return mensaje;

	}
}
