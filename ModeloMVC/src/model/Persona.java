package model;

import java.util.ArrayList;



/**
 * Clase en la que se declaran 20 atributos, sus getters and setters.
 * 
 * @author dareyes@externosdeloittemx.com
 * @version 1.0
 * @since 2020-03-11
 */
public class Persona {
	
	//Crear lista para almacenar los registros de Personas.
	public static ArrayList<Persona> listaPersonas = new ArrayList<Persona>();
	

	// Declaracion de 20 atributos para una persona
	private String nombre;
	private String apellido1;
	private String apellido2;
	private String atributo9;
	private String genero;
	private String rfc;
	private String carrera;
	private int edad, hermanos, hermanas, atributo1, atributo2, atributo3, atributo4, atributo5, atributo6, atributo7,
			atributo8;
	private double peso, estatura;

	/**
	 * Constructor para la clase Entity
	 * 
	 * @param nombre    Debe ser de tipo String.
	 * @param apellido1 Debe ser de tipo String.
	 * @param apellido2 Debe ser de tipo String.
	 * @param cabello   Debe ser de tipo String.
	 * @param genero    Debe ser de tipo String.
	 * @param rfc       Debe ser de tipo String.
	 * @param carrera   Debe ser de tipo String.
	 * @param edad      Debe ser de tipo int.
	 * @param hermanos  Debe ser de tipo int.
	 * @param hermanas  Debe ser de tipo int.
	 * @param atributo1 Debe ser de tipo int.
	 * @param atributo2 Debe ser de tipo int.
	 * @param atributo3 Debe ser de tipo int.
	 * @param atributo4 Debe ser de tipo int.
	 * @param atributo5 Debe ser de tipo int.
	 * @param atributo6 Debe ser de tipo int.
	 * @param atributo7 Debe ser de tipo int.
	 * @param atributo8 Debe ser de tipo int.
	 * @param peso      Debe ser de tipo double.
	 * @param estatura  Debe ser de tipo double.
	 * @param atributo9 
	 */
	public Persona(String nombre, String apellido1, String apellido2, String genero, String rfc,
			String carrera, int edad, int hermanos, int hermanas, int atributo1, int atributo2, int atributo3,
			int atributo4, int atributo5, int atributo6, int atributo7, int atributo8, double peso, double estatura, String atributo9) {
		super();
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.atributo9 = atributo9;
		this.genero = genero;
		this.rfc = rfc;
		this.carrera = carrera;
		this.edad = edad;
		this.hermanos = hermanos;
		this.hermanas = hermanas;
		this.atributo1 = atributo1;
		this.atributo2 = atributo2;
		this.atributo3 = atributo3;
		this.atributo4 = atributo4;
		this.atributo5 = atributo5;
		this.atributo6 = atributo6;
		this.atributo7 = atributo7;
		this.atributo8 = atributo8;
		this.peso = peso;
		this.estatura = estatura;
		listaPersonas.add(this);
	}
	
	public Persona() {
		
	}

	// Getters and Setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getAtributo9() {
		return atributo9;
	}

	public void setAtributo9(String atributo9) {
		this.atributo9 = atributo9;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getCarrera() {
		return carrera;
	}

	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public int getHermanos() {
		return hermanos;
	}

	public void setHermanos(int hermanos) {
		this.hermanos = hermanos;
	}

	public int getHermanas() {
		return hermanas;
	}

	public void setHermanas(int hermanas) {
		this.hermanas = hermanas;
	}

	public int getAtributo1() {
		return atributo1;
	}

	public void setAtributo1(int atributo1) {
		this.atributo1 = atributo1;
	}

	public int getAtributo2() {
		return atributo2;
	}

	public void setAtributo2(int atributo2) {
		this.atributo2 = atributo2;
	}

	public int getAtributo3() {
		return atributo3;
	}

	public void setAtributo3(int atributo3) {
		this.atributo3 = atributo3;
	}

	public int getAtributo4() {
		return atributo4;
	}

	public void setAtributo4(int atributo4) {
		this.atributo4 = atributo4;
	}

	public int getAtributo5() {
		return atributo5;
	}

	public void setAtributo5(int atributo5) {
		this.atributo5 = atributo5;
	}

	public int getAtributo6() {
		return atributo6;
	}

	public void setAtributo6(int atributo6) {
		this.atributo6 = atributo6;
	}

	public int getAtributo7() {
		return atributo7;
	}

	public void setAtributo7(int atributo7) {
		this.atributo7 = atributo7;
	}

	public int getAtributo8() {
		return atributo8;
	}

	public void setAtributo8(int atributo8) {
		this.atributo8 = atributo8;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getEstatura() {
		return estatura;
	}

	public void setEstatura(double estatura) {
		this.estatura = estatura;
	}

}
